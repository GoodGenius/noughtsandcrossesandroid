﻿package noughtsAndCrosses.math;

import java.util.*;


public class Matrix <E> implements MatrixReader<E> {
	public Matrix(int rows, int columns, E defaultValue) {
		super();
		
		ArrayList<E> curRow;
		
		this.rows = rows;
		this.columns = columns;
		this.cells = new ArrayList<ArrayList<E>>(rows);
		this.defaultValue = defaultValue; 
		
		for (int i = 0; i < rows; ++i) {
			curRow = new ArrayList<E>(columns);
			cells.add(curRow);
			
			for (int j = 0; j < columns; ++j) {
				curRow.add(defaultValue);
			}
		}
	}
	
	public int rowsCount() {
		return rows;
	}
	public int columnsCount() {
		return columns;
	}
	
	public E get(int cellX, int cellY)
			throws ArrayIndexOutOfBoundsException {
		if (cellX < 0 || cellX >= columns) {
			throw new ArrayIndexOutOfBoundsException(cellX);
		}
		
		if	(cellY < 0 || cellY >= rows) {
			throw new ArrayIndexOutOfBoundsException(cellY);
		}
		
		return cells.get(cellY).get(cellX); // Y and X are changed, it is correct
	}
	
	public E get(PointInt cell)
			throws ArrayIndexOutOfBoundsException {
		int cellX = cell.getX();
		int cellY = cell.getY();
		
		E result = get(cellX, cellY);
		
		return result;
	}
	
	public void set(int cellX, int cellY, E value)
			throws ArrayIndexOutOfBoundsException {
		
		if (cellX < 0 || cellX >= columns) {
			throw new ArrayIndexOutOfBoundsException(cellX);
		}
		
		if	(cellY < 0 || cellY >= rows) {
			throw new ArrayIndexOutOfBoundsException(cellY);
		}
		
		cells.get(cellY).set(cellX, value); // Y and X are changed, it is correct
	}
	
	public void set(PointInt cell, E value)
			throws ArrayIndexOutOfBoundsException {
		int cellX = cell.getX();
		int cellY = cell.getY();
		
		set(cellX, cellY, value);
	}
	
	// Return value is positive or zero, if parameters are correct. 
	private static int getMaxOffsetXY(int maxXY, int cellXY, int vectorXY) {
		int maxOffsetXY = 0;
		
		switch (vectorXY) {
		case 1:
			// (columnsCount - cellX)/vectorX, but vectorX == 1
			maxOffsetXY = maxXY - cellXY - 1;
			break;
		case -1:
			// (-cellX/vectorX), but vectorX == -1
			maxOffsetXY = cellXY;
			break;
		}
		
		return maxOffsetXY;
	}
	
	private int getMaxOffsetForVector(int cellX, int cellY, int vectorX, int vectorY) {
	
		int maxOffset = 0;
		
		if (vectorY == 0) {
			maxOffset = getMaxOffsetXY(columns, cellX, vectorX);
		}
		else if (vectorX == 0) {
			maxOffset = getMaxOffsetXY(rows, cellY, vectorY); 			
		}
		else {
			int maxOffsetX = getMaxOffsetXY(columns, cellX, vectorX);
			int maxOffsetY = getMaxOffsetXY(rows, cellY, vectorY);
			
			maxOffset = Math.min(maxOffsetX, maxOffsetY);
		}
		
		return maxOffset;
	}
	
	// Return maximum length of ray from cell in direction of lineVecctor
	// before it crosses one of matrix borders.
	// multiplier must be 1 or -1
	public int getMaxVectorOffset(PointInt cell, CellsLineType lineType, int multiplier) {
		VectorInt lineVector = CellsLine.getLineVector(lineType);
		int cellX = cell.getX();
		int cellY = cell.getY();
		int vectorX = multiplier * lineVector.getX();
		int vectorY = multiplier * lineVector.getY();
		
		int maxOffset = getMaxOffsetForVector(cellX, cellY, vectorX, vectorY);
		
		return maxOffset;
	}
	
	public void Extend(int addRows, int addColumns)
		throws NegativeArraySizeException
	{
		if (addRows < 0 || addColumns < 0) {
			throw new NegativeArraySizeException("Arguments of Matrix.Extend(...) cannot be negative");
		}
		
		int newRows = rows + addRows;
		int newColumns = columns + addColumns;
		ArrayList<E> curRow;
		
		// Add columns to existing rows
		for (int i = 0; i < rows; ++i) {
			curRow = cells.get(i);
			curRow.ensureCapacity(newColumns);
			for (int j = columns; j < newColumns; ++j) {
				curRow.set(j, defaultValue);
			}
			
			cells.add(curRow);
		}
		
		// Add new rows
		cells.ensureCapacity(newRows);
		for (int i = rows; i < newRows; ++i) {
			curRow = new ArrayList<E>(newColumns);
			for (int j = 0; j < newColumns; ++j) {
				curRow.set(j, defaultValue);
			}
			
			cells.add(curRow);
		}
	}
	
	private int rows = 0;
	private int columns = 0;
	private ArrayList<ArrayList<E>> cells;
	private E defaultValue;
}
