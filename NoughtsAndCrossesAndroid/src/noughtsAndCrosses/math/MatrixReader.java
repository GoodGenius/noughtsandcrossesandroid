package noughtsAndCrosses.math;

public interface MatrixReader<E> {
	int rowsCount();
	int columnsCount();
	E get(int cellX, int cellY);
	E get(PointInt cell);
	int getMaxVectorOffset(PointInt cell, CellsLineType lineType, int multiplier);
}
