package noughtsAndCrosses.math;

public class PointInt implements Cloneable {
	public PointInt(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public PointInt(PointInt point) {
		x = point.x;
		y = point.y;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void setXY(PointInt point) {
		x = point.x;
		y = point.y;
	}
	
	/*
	public PointInt add(VectorInt vector, int n) {
		int newX = x + n*vector.getX();
		int newY = y + n*vector.getY();
		PointInt newPoint = new PointInt(newX, newY); 
		
		return newPoint;
	}
	*/
	
	public void add(VectorInt vector, int n) {
		x += n*vector.getX();
		y += n*vector.getY();
	}
	
	public boolean equals(PointInt obj) {
		boolean result;

		if (getClass() != obj.getClass()) {
			result = false;
		}
		else if (this == obj) {
			result = true;
		}
		else {
			result = (x == obj.x && y == obj.y);
		}
		
		return result;
	}
	
	public int hashCode() {
		// Coefficients are prime numbers
		int code = 7*x + 47*y;
		
		return code;
	}
	
	public PointInt clone()
	{
		PointInt newPoint = null;
		
		try {
			newPoint = (PointInt)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return newPoint;
	}
	
	private int x, y;
}
