package noughtsAndCrosses.math;

// This class is used to protect real matrix from modifications.
// It ensures that only reading is possible.

public class MatrixReaderImpl<E> implements MatrixReader<E> {
	public MatrixReaderImpl(MatrixReader<E> matrix) {
		this.matrix = matrix;
	}
	
	public int rowsCount() {
		return matrix.rowsCount();
	}
	public int columnsCount() {
		return matrix.columnsCount();
	}
	public E get(int cellX, int cellY) {
		return matrix.get(cellX, cellY);
	}
	public E get(PointInt cell) {
		return matrix.get(cell);
	}
	public int getMaxVectorOffset(PointInt cell, CellsLineType lineType, int multiplier) {
		return matrix.getMaxVectorOffset(cell, lineType, multiplier);
	}
	
	private MatrixReader<E> matrix;
}
