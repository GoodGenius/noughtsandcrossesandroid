package noughtsAndCrosses.math;

public enum CellsLineType {
	HORIZONTAL
	,VERTICAL
	,DIAGONAL1 // from (0, 0) to (columnsCount, rowsCount) 
	,DIAGONAL2 // from (0, columnsCount) to (0, rowsCount)
}
