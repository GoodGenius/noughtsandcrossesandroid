package noughtsAndCrosses.math;

public class PointOnLine {
	public boolean isOnLine() {
		return onLine;
	}
	public boolean isOnSegment() {
		return onSegment;
	}
	public int getRangeFromStart() {
		return rangeFromStart;
	}
	public boolean doesExtendLine() {
		return extendsLine;
	}
	
	public void clearState() {
		onLine = false;
		onSegment = false;
		extendsLine = false;
		rangeFromStart = 0;
	}
	
	public void setUpState(CellsLine line, PointInt cell) {
		PointInt startPoint = line.getStartPoint();
		VectorInt lineVector = line.getLineVector();
		int cellX = cell.getX();
		int cellY = cell.getY();
		int startX = startPoint.getX();
		int startY = startPoint.getY();
		int vectorX = lineVector.getX(); // Can be: -1, 0, 1
		int vectorY = lineVector.getY(); // Can be: -1, 0, 1
		int lineLength = line.getLength();
		
		clearState();
		
		// isOnLine = (cellX - startX)/vectorX - (cellY - startY)/vectorY == 0
		// The equation multiplied by vectorX*vectorY become: 
		onLine = ((cellX - startX)*vectorY - (cellY - startY)*vectorX == 0);
		
		if (onLine) {
			if (vectorX != 0) {
				// if vectorX is -1 or 1 we can replace "/" by "*"
				rangeFromStart = (cellX - startX)*vectorX;
			}
			else if (vectorY != 0) {
				rangeFromStart = (cellY - startY)*vectorY;
			}
			
			onSegment = (rangeFromStart >= 0 && rangeFromStart <= lineLength-1);
			extendsLine = (rangeFromStart == -1 || rangeFromStart == lineLength);
		}
	}
	
	private boolean onLine = false;
	private boolean onSegment = false;
	private boolean extendsLine = false;
	private int rangeFromStart = 0;
}
