package noughtsAndCrosses.math;

import java.util.*;

import noughtsAndCrosses.gameModel.*;

public class CellsLine {
	public void setUpLine(CellsLineType lineType, FigureType figureType, PointInt startPoint, int length) {
		this.lineType = lineType;
		this.figureType = figureType;
		
		//this.startPoint = new PointInt(startPoint);
		this.startPoint = startPoint;
		
		this.length = length;
		lineVector = getLineVector(lineType);
	}
	
	public void setUpLine(CellsLine line) {
		lineType = line.lineType;
		figureType = line.figureType;
		
		startPoint = new PointInt(line.startPoint);
		
		length = line.length;
		lineVector = getLineVector(lineType);
	}
	
	public CellsLineType getLineType() {
		return lineType;
	}
	
	public FigureType getFigureType() {
		return figureType;
	}
	
	public PointInt getStartPoint() {
		return startPoint;
	}
	
	public int getLength() {
		return length;
	}
	
	public boolean equals(CellsLine obj) {
		boolean result;

		if (getClass() != obj.getClass()) {
			result = false;
		}
		else if (this == obj) {
			result = true;
		}
		else {
			result = (startPoint.equals(obj.startPoint)
					&& length == obj.length
					&& figureType == obj.figureType
					&& lineType == obj.lineType
					);
		}
		
		return result;
	}
	
	public int hashCode() {
		// Coefficients are prime numbers
		int code = lineType.hashCode() + 7*figureType.hashCode() + 11*startPoint.hashCode() + 17*length;
		
		return code;
	}
	
	public VectorInt getLineVector() {
		return lineVector;
	}
	
	public boolean containsLine(CellsLine otherLine) {
		PointOnLine pointState = new PointOnLine();
		PointInt endPoint = new PointInt(otherLine.startPoint);
		
		boolean contains = (otherLine.lineType == lineType);
		
		if (contains) {
			// Does this line contains start point of the other line?
			pointState.setUpState(this, otherLine.startPoint);
			contains = contains && pointState.isOnSegment();
		}
		
		if (contains) {
			// Does this line contains end point of the other line?
			endPoint.add(otherLine.lineVector, otherLine.length);
			
			pointState.setUpState(this, endPoint);
			contains = contains && pointState.isOnSegment();
		}
		
		return contains;
	}
	
	private FigureType figureType = FigureType.EMPTY;
	private CellsLineType lineType = null;
	private PointInt startPoint = null;
	private int length = 0;
	private VectorInt lineVector = null;
	
	private static final HashMap<CellsLineType, VectorInt> lineVectorsMap;
	// Static initializer
	static {
		lineVectorsMap = new HashMap<CellsLineType, VectorInt>(4, 1f);
		lineVectorsMap.put(CellsLineType.HORIZONTAL, new VectorInt(1, 0));
		lineVectorsMap.put(CellsLineType.VERTICAL, new VectorInt(0, 1));
		lineVectorsMap.put(CellsLineType.DIAGONAL1, new VectorInt(1, 1));
		lineVectorsMap.put(CellsLineType.DIAGONAL2, new VectorInt(-1, 1));
	}
	
	public static VectorInt getLineVector(CellsLineType lineType) {
		return lineVectorsMap.get(lineType);
	}
}
