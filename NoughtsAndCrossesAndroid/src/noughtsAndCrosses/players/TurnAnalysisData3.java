package noughtsAndCrosses.players;

import noughtsAndCrosses.gameModel.GameModel;
import noughtsAndCrosses.math.*;

class TurnAnalysisData3 {
	PointInt getOurBestCell() {
		return ourBestCell;
	}
	CellMetric3 getOurBestCellMetric() {
		return ourBestCellMetric;
	}
	
	void setOurBestCell(PointInt newCell, CellMetric3 newMetric) {
		boolean doUpdate = false;
		
		if (ourBestCellMetric == null) {
			doUpdate = true;
		} 
		else if (newMetric.compareTo(ourBestCellMetric) > 0) {
			doUpdate = true;
		}
		
		if (doUpdate) {
			ourBestCellSet = true;
			ourBestCell.setXY(newCell);
			ourBestCellMetric = newMetric;
		} 
	}
	
	PointInt getBestBreakCell() {
		return bestBreakCell;
	}
	CellMetric3 getBestBreakMetric() {
		return bestBreakMetric;
	}
	
	void setBestBreakCell(PointInt newCell, CellMetric3 newMetric) {
		boolean doUpdate = false;
		
		if (bestBreakMetric == null) {
			doUpdate = true;
		} 
		else if (newMetric.compareTo(bestBreakMetric) > 0) {
			doUpdate = true;
		}
		
		if (doUpdate) {
			bestBreakCellSet = true;
			bestBreakCell.setXY(newCell);
			bestBreakMetric = newMetric;
		} 
	}
	
	private boolean isLineHighlyDangerous(LineMetric3 metric) {
		int subsegmentLen = metric.getOurSubsegmentLen();
		boolean isDangerous = (subsegmentLen >= GameModel.WIN_LINE_LENGTH
			|| subsegmentLen == GameModel.WIN_LINE_LENGTH - 1 && metric.isOpen());
		
		return isDangerous;
	} 
	
	private boolean isLineDangerous(LineMetric3 metric) {
		int subsegmentLen = metric.getOurSubsegmentLen();
		boolean isDangerous = (subsegmentLen >= GameModel.WIN_LINE_LENGTH - 1);
		
		return isDangerous;
	} 
	
	private boolean isBreakCellGoodEnough() {
		LineMetric3 metricLine1, metricLine2;
		int linesCount = bestBreakMetric.linesCount(); 
		boolean isGoodEnough = false;
		
		if (linesCount > 0) {
			bestBreakMetric.sort();
			metricLine1 = bestBreakMetric.getLine(0);
			
			if (isLineHighlyDangerous(metricLine1)) {
				isGoodEnough = true;
			}
		}
		
		if (!isGoodEnough && linesCount >= 2) {
			metricLine1 = bestBreakMetric.getLine(0);
			metricLine2 = bestBreakMetric.getLine(1);
			
			if (isLineDangerous(metricLine1) && isLineDangerous(metricLine2)) {
				isGoodEnough = true;
			}
		}
		
		return isGoodEnough;
	}
	
	PointInt getBestTurn() {
		PointInt result = null;
		
		if (!ourBestCellSet && !bestBreakCellSet) {
			// result is already null
		}
		else if (ourBestCellSet && !bestBreakCellSet) {
			result = ourBestCell;
		}
		else if (!ourBestCellSet && bestBreakCellSet) {
			result = bestBreakCell;
		}
		else if (ourBestCellMetric.compareTo(bestBreakMetric) >= 0) {
			result = ourBestCell;
		}
		else {
			result = (isBreakCellGoodEnough() ? bestBreakCell : ourBestCell);
		}
		
		return result;
	}
	
	private boolean ourBestCellSet = false;
	private PointInt ourBestCell = new PointInt(-1, -1);
	private CellMetric3 ourBestCellMetric = null;
	
	private boolean bestBreakCellSet = false;
	private PointInt bestBreakCell = new PointInt(-1, -1);
	private CellMetric3 bestBreakMetric = null;
}
