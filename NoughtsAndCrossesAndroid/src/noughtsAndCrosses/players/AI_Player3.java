package noughtsAndCrosses.players;

import java.util.*;

import noughtsAndCrosses.gameModel.*;
import noughtsAndCrosses.math.*;

public class AI_Player3 implements Player {

	public void initialize(MatrixReader<FigureType> matrix, FigureType playerFigures, Interruptable threadContainer) {
		
		this.matrix = matrix;
		this.playerFigures = playerFigures;
		this.threadContainer = threadContainer;
		
		rowsCount = matrix.rowsCount();
		columnsCount = matrix.columnsCount();
		
		//analyzer = new BasicGameAnalyzer(matrix);
		tools = new AI_Tools(matrix, threadContainer);
		
		opponentFigures = tools.getOpponentFigures(playerFigures);
	}
	
	public PointInt makeTurn(PointInt opponentTurn, int turnNumber) {
		PointInt curCell;
		//CellMetric3 metric = new CellMetric3();
		FigureType cellFigure;
		TurnAnalysisData3 analysisData = new TurnAnalysisData3();
		int cellX, cellY;
		
		switch (turnNumber) {
		case 1:
			curCell = tools.getFirstTurn();
			analysisData.setOurBestCell(curCell, null);
			break;
		case 2:
			curCell = tools.getSecondTurn();
			if (curCell != null) {
				analysisData.setOurBestCell(curCell, null);
			}
			break;
		default:
			cellY = 0;
			
			while (cellY < rowsCount && !threadContainer.isInterrupted()) {
				cellX = 0;
				
				while (cellX < columnsCount && !threadContainer.isInterrupted()) {
					curCell = new PointInt(cellX, cellY);
					
					cellFigure = matrix.get(cellX, cellY);
					
					if (cellFigure == FigureType.EMPTY) {
						analyseCell(analysisData, curCell);
					}
					++cellX;
				}
				++cellY;
			}
			break;
		}
		
		return analysisData.getBestTurn();
	}
	
	private void analyseCell(TurnAnalysisData3 analysisData, PointInt cell) {
		CellMetric3 ourMetric = new CellMetric3();
		CellMetric3 breakMetric = new CellMetric3();
		LineMetric3 curLineMetric;
		
		for (CellsLineType lineType : allLineTypes) {
			curLineMetric = evaluateLine(lineType, cell, playerFigures);
			ourMetric.addLine(curLineMetric);
			
			curLineMetric = evaluateLine(lineType, cell, opponentFigures);
			breakMetric.addLine(curLineMetric);
		}
		
		analysisData.setOurBestCell(cell, ourMetric);
		analysisData.setBestBreakCell(cell, breakMetric);
	}
	
	private LineMetric3 evaluateLine(CellsLineType lineType, PointInt initialCell, FigureType cellFigure) {
		int minOffset = -matrix.getMaxVectorOffset(initialCell, lineType, -1);
		int maxOffset = matrix.getMaxVectorOffset(initialCell, lineType, 1);
		
		minOffset = Math.max(minOffset, -GameModel.WIN_LINE_LENGTH + 1); // max of 2 negative values
		maxOffset = Math.min(maxOffset, GameModel.WIN_LINE_LENGTH - 1);
		
		ArrayList<FigureType> figuresLine = new ArrayList<FigureType>(maxOffset - minOffset + 1);
		FigureType curFigure;
		int initialCellIndex = -1;
		
		int curOffset = minOffset;
		VectorInt lineVector = CellsLine.getLineVector(lineType);
		PointInt curCell = new PointInt(initialCell);
		LineMetric3 lineMetric;
		
		curCell.add(lineVector, minOffset); // minOffset <= 0
		
		while (curOffset <= maxOffset) {
			if (curOffset == 0) {
				curFigure = cellFigure;
				initialCellIndex = figuresLine.size();
			}
			else {
				curFigure = matrix.get(curCell);
			}
			
			figuresLine.add(curFigure);
			
			++curOffset;
			curCell.add(lineVector, 1);
		}
		
		lineMetric = new LineMetric3();
		lineMetric.analyzeLine(figuresLine, initialCellIndex);
		
		return lineMetric;
	}
	
	private FigureType playerFigures;
	private FigureType opponentFigures;
	private MatrixReader<FigureType> matrix;
	private int columnsCount;
	private int rowsCount;
	private Interruptable threadContainer;
	//private BasicGameAnalyzer analyzer;
	private AI_Tools tools;
	private CellsLineType[] allLineTypes = CellsLineType.values();
}
