package noughtsAndCrosses.players;

public interface Interruptable {
	boolean isInterrupted();
}
