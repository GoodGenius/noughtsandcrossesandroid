package noughtsAndCrosses.players;

import noughtsAndCrosses.gameModel.*;
import noughtsAndCrosses.math.*;

class CellInMatrix extends PointInt implements Comparable<CellInMatrix>{
	public CellInMatrix(MatrixReader<FigureType> matrix, int x, int y) {
		super(x, y);
		
		int columns = matrix.columnsCount();
		int rows = matrix.rowsCount();
		
		// Manhattan distance metric
		rangeFromCenter = Math.abs(columns/2.0 - x) + Math.abs(rows/2.0 - y); 
	}
	
	public double getRangeFromCenter() {
		return rangeFromCenter;
	} 
	
	public int compareTo(CellInMatrix cell) {
		int result = 0;
		double cellRangeFromCenter = cell.getRangeFromCenter();
		
		if (cell.getX() == getX() && cell.getY() == getY()
				|| cellRangeFromCenter == rangeFromCenter)
		{
			// Equal
		}
		else if (cellRangeFromCenter > rangeFromCenter) {
			result = -1;
		}
		else {
			result = 1;
		}
		
		return result;
	}
	
	private double rangeFromCenter = 0.0;
}
