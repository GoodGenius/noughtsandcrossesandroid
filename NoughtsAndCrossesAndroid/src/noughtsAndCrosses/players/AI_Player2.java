package noughtsAndCrosses.players;

import noughtsAndCrosses.gameModel.BasicGameAnalyzer;
import noughtsAndCrosses.gameModel.FigureType;
import noughtsAndCrosses.gameModel.GameModel;
import noughtsAndCrosses.math.CellsLine;
import noughtsAndCrosses.math.CellsLineType;
import noughtsAndCrosses.math.MatrixReader;
import noughtsAndCrosses.math.PointInt;

public class AI_Player2 implements Player {

	public void initialize(MatrixReader<FigureType> matrix, FigureType playerFigures, Interruptable threadContainer) {
		
		this.matrix = matrix;
		this.playerFigures = playerFigures;
		this.threadContainer = threadContainer;
		
		rowsCount = matrix.rowsCount();
		columnsCount = matrix.columnsCount();
		
		analyser = new BasicGameAnalyzer(matrix);
		tools = new AI_Tools(matrix, threadContainer);
		
		opponentsFigures = tools.getOpponentFigures(playerFigures);
	}
	
	// Overridden in AI_Player2_Bugged
	protected boolean areBothPositive(int exStart, int exEnd) {
		return (exStart > 0 && exEnd > 0);
	}
	
	private void analyseCell(TurnAnalysisData2 analysisData, PointInt curCell) {
		CellsLine curLine = new CellsLine();
		LineInMatrix lineInMatrix = new LineInMatrix();
		int curMaxLen = analysisData.getMaxLineLength();
		int lineLen, linePosLen;
		int oppMaxLen = analysisData.getMaxOpponentLen();
		int oppExStart = 0;
		int oppExEnd = 0;
		
		for (CellsLineType lineType : allLineTypes) {
			analyser.findLine(lineType, curCell, playerFigures, curLine);
			lineLen = curLine.getLength();
			
			lineInMatrix.setUpState(matrix, curLine);
			linePosLen = lineInMatrix.getMaxPossibleLen(); 
			
			if (lineLen > curMaxLen) {
				// Else find any empty cell
				if (linePosLen >= GameModel.WIN_LINE_LENGTH) {
					curMaxLen = lineLen;
					analysisData.setMaxLineLength(lineLen);
					analysisData.setMaxPossibleLineLength(linePosLen);
				}
				
				analysisData.setOurCell(curCell);
			}
			
			// Analyze opponent's possible line
			analyser.findLine(lineType, curCell, opponentsFigures, curLine);
			lineLen = curLine.getLength();
			
			lineInMatrix.setUpState(matrix, curLine);
			linePosLen = lineInMatrix.getMaxPossibleLen();
			oppExStart = lineInMatrix.getExtentFromStart();
			oppExEnd = lineInMatrix.getExtentFromEnd();
			
			if (lineLen > oppMaxLen && linePosLen >= GameModel.WIN_LINE_LENGTH
					// We already put additional opponent's figure, so WIN_LINE_LENGTH - 1 means WIN_LINE_LENGTH - 2
					// Exclude double blocking line of 3!!!
					//&& (lineLen == GameModel.WIN_LINE_LENGTH - 1 && (oppExStart > 0 || oppExEnd > 0)
					&& (lineLen == GameModel.WIN_LINE_LENGTH - 1
					&& this.areBothPositive(oppExStart, oppExEnd)
						|| lineLen == GameModel.WIN_LINE_LENGTH)) {
				
				oppMaxLen = lineLen;
				analysisData.setMaxOpponentLen(lineLen);
				analysisData.setBreakCell(curCell);
			}
		}
	}
	
	public PointInt makeTurn(PointInt opponentTurn, int turnNumber) {
		
		FigureType cellFigure;
		
		PointInt ourTurn;
		TurnAnalysisData2 analysisData = new TurnAnalysisData2();
		int cellX, cellY;
		
		PointInt curCell = new PointInt(0, 0);
		
		switch (turnNumber) {
		case 1:
			ourTurn = tools.getFirstTurn();
			analysisData.setOurCell(ourTurn);
			break;
		case 2:
			ourTurn = tools.getSecondTurn();
			if (ourTurn != null) {
				analysisData.setOurCell(ourTurn);
			}
			break;
		default:
			cellY = 0;
			
			while (cellY < rowsCount && !threadContainer.isInterrupted()) {
				curCell.setY(cellY);
				cellX = 0;
				
				while (cellX < columnsCount && !threadContainer.isInterrupted()) {
					curCell.setX(cellX);
					cellFigure = matrix.get(cellX, cellY);
					
					if (cellFigure == FigureType.EMPTY) {
						analyseCell(analysisData, curCell);
					}
					++cellX;
				}
				++cellY;
			}
			break;
		}
		
		if (!analysisData.isBreakCellSet()
				|| analysisData.getMaxLineLength() == GameModel.WIN_LINE_LENGTH) {
			
			ourTurn = analysisData.getOurCell(); 
		}
		else {
			ourTurn = analysisData.getBreakCell();
		}
		
		return ourTurn;
	}

	private FigureType playerFigures;
	private FigureType opponentsFigures;
	private MatrixReader<FigureType> matrix;
	private int columnsCount;
	private int rowsCount;
	private Interruptable threadContainer;
	private BasicGameAnalyzer analyser;
	private AI_Tools tools;
	private CellsLineType[] allLineTypes = CellsLineType.values();
}
