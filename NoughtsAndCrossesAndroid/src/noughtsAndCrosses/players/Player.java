package noughtsAndCrosses.players;

import noughtsAndCrosses.gameModel.*;
import noughtsAndCrosses.math.*;

public interface Player {
	void initialize(MatrixReader<FigureType> matrix, FigureType playerFigures, Interruptable threadContainer);
	
	// opponentsTurn - opponent's move coordinates
	// Can be null for first turn or if thread was interrupted
	// Executed in game model thread, called through Model -> Player 
	PointInt makeTurn(PointInt opponentTurn, int turnNumber);
}
