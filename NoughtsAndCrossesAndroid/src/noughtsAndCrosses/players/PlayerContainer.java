package noughtsAndCrosses.players;

public class PlayerContainer {
	public PlayerContainer(Player player, PlayerType playerType) {
		this.playerType = playerType;
		
		if (playerType == PlayerType.LOCAL_HUMAN) {
			this.player = null;
		}
		else {
			this.player = player;
		}
	}
	
	public PlayerType getPlayerType() {
		return playerType;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	private final PlayerType playerType;
	private final Player player;
}
