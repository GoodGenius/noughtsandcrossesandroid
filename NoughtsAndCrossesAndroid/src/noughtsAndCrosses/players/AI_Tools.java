package noughtsAndCrosses.players;

import java.util.*;

import noughtsAndCrosses.gameModel.*;
import noughtsAndCrosses.math.*;

class AI_Tools {
	
	public AI_Tools(MatrixReader<FigureType> matrix, Interruptable threadContainer) {
		this.matrix = matrix;
		
		rowsCount = matrix.rowsCount();
		columnsCount = matrix.columnsCount();
		
		this.threadContainer = threadContainer;
	}
	
	private void addCellIfEmpty(ArrayList<CellInMatrix> emptyCellsAround, int x, int y) {
		if (x >= 0 && y >= 0 && x < columnsCount && y < rowsCount  
				&& matrix.get(x, y) == FigureType.EMPTY) {
			emptyCellsAround.add(new CellInMatrix(matrix, x, y));
		}
	}
	
	public PointInt findEmptyAround(int cellX, int cellY) {
		ArrayList<CellInMatrix> emptyCellsAround = new ArrayList<CellInMatrix>(8);
		CellInMatrix ourTurn = null;
		
		addCellIfEmpty(emptyCellsAround, cellX - 1, cellY - 1);
		addCellIfEmpty(emptyCellsAround, cellX,		cellY - 1);
		addCellIfEmpty(emptyCellsAround, cellX + 1, cellY - 1);
		addCellIfEmpty(emptyCellsAround, cellX - 1, cellY);
		addCellIfEmpty(emptyCellsAround, cellX + 1, cellY);
		addCellIfEmpty(emptyCellsAround, cellX - 1, cellY + 1);
		addCellIfEmpty(emptyCellsAround, cellX,		cellY + 1);
		addCellIfEmpty(emptyCellsAround, cellX + 1, cellY + 1);
		
		// Find empty cell around closest to the matrix center
		java.util.Collections.sort(emptyCellsAround);
		
		if (emptyCellsAround.size() > 0) {
			ourTurn = emptyCellsAround.get(0);
		}
		
		return ourTurn;
	}
	
	public PointInt getFirstTurn() {
		int ourTurnX, ourTurnY;
		PointInt ourTurn;
		
		
		// Place our figure at field center
		ourTurnX = (columnsCount - 1)/2;
		ourTurnY = (rowsCount - 1)/2;
		
		ourTurn = new PointInt(ourTurnX, ourTurnY);
		
		return ourTurn;
	}
	
	public PointInt getSecondTurn() {
		int cellX, cellY;
		FigureType cellFigure;
		PointInt ourTurn = null;
		
		// Find the only opponent's figure and place our close to it
		cellY = 0;
		
		while (cellY < rowsCount && ourTurn == null && !threadContainer.isInterrupted()) {
			cellX = 0;
			
			while (cellX < columnsCount && ourTurn == null && !threadContainer.isInterrupted()) {
				cellFigure = matrix.get(cellX, cellY);
				
				if (cellFigure != FigureType.EMPTY) {
					ourTurn = findEmptyAround(cellX, cellY);
				}
				++cellX;
			}
			++cellY;
		}
		
		return ourTurn;
	}
	
	public FigureType getOpponentFigures(FigureType ourFigures) {
		FigureType opponent = null;
		
		switch (ourFigures) {
		case CROSS:
			opponent = FigureType.NOUGHT;
			break;
		case NOUGHT:
			opponent = FigureType.CROSS;
			break;
		default:
			opponent = FigureType.EMPTY;
			break;
		}
		
		return opponent;
	}
	
	private MatrixReader<FigureType> matrix;
	private int columnsCount;
	private int rowsCount;
	private Interruptable threadContainer;
}
