package noughtsAndCrosses.players;

import noughtsAndCrosses.math.*;

class TurnAnalysisData2 extends TurnAnalysisData {
	
	public int getMaxOpponentLen() {
		return maxOpponentLen;
	}
	public void setMaxOpponentLen(int maxOpponentLen) {
		this.maxOpponentLen = maxOpponentLen;
	}
	public int getBreakCellX() {
		return breakCellX;
	}
	public int getBreakCellY() {
		return breakCellY;
	}
	public PointInt getBreakCell() {
		PointInt ourTurn = new PointInt(breakCellX, breakCellY);
		
		return ourTurn;
	}
	public boolean isBreakCellSet() {
		return isBreakCellSet;
	}
	public void setBreakCell(PointInt breakCell) {
		breakCellX = breakCell.getX();
		breakCellY = breakCell.getY();
		
		isBreakCellSet = true;
	}
	
	private boolean isBreakCellSet = false;
	private int breakCellX = -1;
	private int breakCellY = -1;
	private int maxOpponentLen = 0;
}
