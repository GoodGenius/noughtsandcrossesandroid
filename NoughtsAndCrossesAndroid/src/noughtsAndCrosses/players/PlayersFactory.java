package noughtsAndCrosses.players;

public class PlayersFactory {
	public Player createPlayer(PlayerType type) {
		Player result = null;
		
		if (type != null) {
			switch (type) {
			case AI1:
				result = new AI_Player1();
				break;
			case AI2:
				result = new AI_Player2();
				break;
			case AI2_BUGGED:
				result = new AI_Player2_Bugged();
				break;
			case AI3:
				result = new AI_Player3();
				break;
			case LOCAL_HUMAN:
				// result is already null
				break;
			}
		}
		
		return result;
	}
}
