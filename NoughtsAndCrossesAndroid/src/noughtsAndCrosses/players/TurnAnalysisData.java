package noughtsAndCrosses.players;

import noughtsAndCrosses.math.*;

class TurnAnalysisData {
	public int getOurCellX() {
		return ourCellX;
	}
	public int getOurCellY() {
		return ourCellY;
	}
	public void setOurCellX(int ourCellX) {
		this.ourCellX = ourCellX;
	}
	public void setOurCellY(int ourCellY) {
		this.ourCellY = ourCellY;
	}
	public PointInt getOurCell() {
		PointInt ourTurn = new PointInt(ourCellX, ourCellY);
		
		return ourTurn;
	}
	public void setOurCell(PointInt ourCell) {
		ourCellX = ourCell.getX();
		ourCellY = ourCell.getY();
	}
	public int getMaxLineLength() {
		return maxLineLength;
	}
	public void setMaxLineLength(int maxLineLength) {
		this.maxLineLength = maxLineLength;
	}
	public int getMaxPossibleLineLength() {
		return maxPossibleLineLength;
	}
	public void setMaxPossibleLineLength(int maxPossibleLineLength) {
		this.maxPossibleLineLength = maxPossibleLineLength;
	}

	private int ourCellX = -1;
	private int ourCellY = -1;
	private int maxLineLength = 0;
	private int maxPossibleLineLength = 0;
}
