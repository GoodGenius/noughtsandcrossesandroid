package noughtsAndCrosses.players;

import java.util.*;

import noughtsAndCrosses.gameModel.*;

class LineMetric3 implements Comparable<LineMetric3> {
	LineMetric3() {
		metricValues = new ArrayList<Object>(4);
		
		metricValues.add(new Integer(0));
		metricValues.add(new Boolean(false));
		metricValues.add(new Integer(0));
		metricValues.add(new Integer(0));
	}
	
	int getOurSubsegmentLen() {
		return (Integer)metricValues.get(0);
		//return ourSubsegmentLen;
	}
	
	boolean isOpen() {
		return (Boolean)metricValues.get(1);
		//return isOpen;
	}
	
	int getOurCellsCount() {
		return (Integer)metricValues.get(2);
		//return ourCellsCount;
	}

	int getHolesCount() {
		return (Integer)metricValues.get(3);
		//return holesCount;
	}

	public int compareTo(LineMetric3 metric) {
		int result = 0;
		int i = 0;
		int metricsCount = metricValues.size();
		Object curValue1, curValue2;
		ComparisonType compType;
		boolean descending;
		
		
		if (metric == null) {
			throw new NullPointerException("Cannot compare LineMetric3 object with null");
		}
		
		while (i < metricsCount && result == 0) {
			curValue1 = metricValues.get(i);
			curValue2 = metric.metricValues.get(i);
			
			compType = metricCompTypes.get(i); 
			descending = metricCompOrder[i];
			
			switch (compType) {
			case INT:
				if ((Integer)curValue1 > (Integer)curValue2) {
					result = 1;
				}
				else if ((Integer)curValue1 < (Integer)curValue2) {
					result = -1;
				}
				break;
			case BOOL:
				if ((Boolean)curValue1 && !(Boolean)curValue2) {
					result = 1;
				}
				else if (!(Boolean)curValue1 && (Boolean)curValue2) {
					result = -1;
				}
				break;
			}
			
			if (descending && result != 0) {
				result = -result;
			}
			
			++i;
		}
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		LineMetric3 metric = null;
		
		if (obj == null || obj.getClass() != getClass()) {
			
		}
		else {
			metric = (LineMetric3)obj;
			result = metricValues.equals(metric.metricValues);
		}
		
		return result;
	}
	
	@Override
	public int hashCode() {
		return metricValues.hashCode();
	}
	
	public void analyzeLine(ArrayList<FigureType> figuresLine, int initialCellIndex) {
		final FigureType ourFigure = figuresLine.get(initialCellIndex);
		FigureType inPrevFigure = null;
		FigureType curFigure = null;
		FigureType outPrevFigure = null;
		int curIndex = 0;
		int arraySize = figuresLine.size();
		
		int queueLen = 0;
		// FIQ - Figures In the Queue (number of figures)
		int ourFIQ = 0;
		int oppFIQ = 0;
		int maxOurFIQ = 0;
		int holes = 0;
		int minHoles = Integer.MAX_VALUE;
		boolean inHoleStarted = false;
		
		movingSegment.clear();
		
		while (curIndex < arraySize) {
			// Input
			curFigure = figuresLine.get(curIndex);
			
			movingSegment.add(curFigure);
			
			++queueLen;
			if (curFigure == ourFigure) {
				++ourFIQ;
			}
			else if (curFigure != FigureType.EMPTY) {
				++oppFIQ;
			}
			else if (inPrevFigure != null && inPrevFigure != FigureType.EMPTY && !inHoleStarted) {
				inHoleStarted = true; // curFigure == FigureType.EMPTY
			}
			
			if (inHoleStarted && curFigure != FigureType.EMPTY) {
				inHoleStarted = false;
				++holes;
			}
			inPrevFigure = curFigure; 
			
			// Output
			if (queueLen > GameModel.WIN_LINE_LENGTH) {
				curFigure = movingSegment.get(0);
				movingSegment.remove(0);
				
				--queueLen;
				if (curFigure == ourFigure) {
					--ourFIQ;
				}
				else if (curFigure != FigureType.EMPTY) {
					--oppFIQ;
				}
				
				if (outPrevFigure != null && outPrevFigure != FigureType.EMPTY && curFigure == FigureType.EMPTY) {
					--holes;
				}
				
				outPrevFigure = curFigure;
			}
			
			// Analysis
			// Line has 5 cells, there are no opponent figures and number of our figures is greater than maximum 
			if (queueLen == GameModel.WIN_LINE_LENGTH && oppFIQ == 0) {
				if (ourFIQ > maxOurFIQ || ourFIQ == maxOurFIQ && holes < minHoles) {
					maxOurFIQ = ourFIQ;
					minHoles = holes;
				}
			}
			
			++curIndex;
		}
		
		// Setup ourSubsegmentMetric
		analyzeOurSubsegment(figuresLine, initialCellIndex);
		
		metricValues.set(0, ourSubsegmentMetric.len);
		metricValues.set(1, ourSubsegmentMetric.isOpen);
		metricValues.set(2, maxOurFIQ);
		metricValues.set(3, minHoles);
	}
	
	private void analyzeOurSubsegment(ArrayList<FigureType> figuresLine, int initialCellIndex) {
		final FigureType ourFigure = figuresLine.get(initialCellIndex);
		FigureType curFigure = ourFigure;
		int curIndex = initialCellIndex;
		int maxArrayIndex = figuresLine.size() - 1;
		int minOurSegmentIndex = -1;
		int maxOurSegmentIndex = -1;
		int additionalLenNeeded = -1;
		int minExtendedIndex = -1;
		int maxExtendedIndex = -1;
		boolean openLow = false;
		boolean openHigh = false;
		
		while (curIndex > 0 && curFigure == ourFigure) {
			--curIndex;
			curFigure = figuresLine.get(curIndex);
		}
		
		if (curFigure != ourFigure) {
			++curIndex;
		}
		minOurSegmentIndex = curIndex;
		
		curIndex = initialCellIndex;
		curFigure = ourFigure;
		
		while (curIndex < maxArrayIndex && curFigure == ourFigure) {
			++curIndex;
			curFigure = figuresLine.get(curIndex);
		}
		if (curFigure != ourFigure) {
			--curIndex;
		}
		maxOurSegmentIndex = curIndex;
		
		additionalLenNeeded = GameModel.WIN_LINE_LENGTH - maxOurSegmentIndex + minOurSegmentIndex;
		
		if (additionalLenNeeded < 0) {
			additionalLenNeeded = 0;
		}
		
		curIndex = minOurSegmentIndex;
		curFigure = ourFigure;
		minExtendedIndex = Math.max(minOurSegmentIndex - additionalLenNeeded, 0);
		
		while (curIndex > minExtendedIndex && (curFigure == ourFigure || curFigure == FigureType.EMPTY)) {
			--curIndex;
			curFigure = figuresLine.get(curIndex);
		}
		openLow = (curFigure == ourFigure || curFigure == FigureType.EMPTY);
		
		
		curIndex = maxOurSegmentIndex;
		curFigure = ourFigure;
		maxExtendedIndex = Math.min(maxOurSegmentIndex + additionalLenNeeded, maxArrayIndex);
		
		while (curIndex < maxExtendedIndex && (curFigure == ourFigure || curFigure == FigureType.EMPTY)) {
			++curIndex;
			curFigure = figuresLine.get(curIndex);
		}
		openHigh = (curFigure == ourFigure || curFigure == FigureType.EMPTY);
		
		ourSubsegmentMetric.len = maxOurSegmentIndex - minOurSegmentIndex + 1;
		ourSubsegmentMetric.isOpen = openLow && openHigh;
	}
	
	private class SegmentMetric {
		int len = 0;
		boolean isOpen = false;
	}
	
	private enum ComparisonType {
		INT
		,BOOL
	}
	
	private ArrayList<FigureType> movingSegment = new ArrayList<FigureType>(GameModel.WIN_LINE_LENGTH);
	private ArrayList<Object> metricValues;
	private SegmentMetric ourSubsegmentMetric = new SegmentMetric();
	
	private static ArrayList<ComparisonType> metricCompTypes; {
		metricCompTypes = new ArrayList<ComparisonType>(4);
		metricCompTypes.add(ComparisonType.INT); // Length of our Subsegment
		metricCompTypes.add(ComparisonType.BOOL); // isOpen
		metricCompTypes.add(ComparisonType.INT); // Our cells count
		metricCompTypes.add(ComparisonType.INT); // Holes count
	};
	
	private static boolean[] metricCompOrder; {
		metricCompOrder = new boolean[4];
		metricCompOrder[0] = false; // Ascending 
		metricCompOrder[1] = false;
		metricCompOrder[2] = false;
		metricCompOrder[3] = true;	// Descending
	};
}
