package noughtsAndCrosses.players;

import noughtsAndCrosses.gameModel.*;
import noughtsAndCrosses.math.*;

public class LineInMatrix {
	public void clearState() {
		extentFromStart = 0;
		extentFromEnd = 0;
		maxPossibleLen = 0;
	}
	
	public int getExtentFromStart() {
		return extentFromStart; 
	}
	
	public int getExtentFromEnd() {
		return extentFromEnd; 
	}
	
	public int getMaxPossibleLen() {
		return maxPossibleLen;
	}
	
	// line have to be complete, before calling this function,
	// e.g. it is not extendible by the same type figures as it consists of.
	public void setUpState(MatrixReader<FigureType> matrix, CellsLine line) {
		VectorInt lineVector = line.getLineVector();
		PointInt initialCell = line.getStartPoint();
		CellsLineType lineType = line.getLineType();
		FigureType lineFigure = line.getFigureType();
		int lineLength = line.getLength();
		
		int startOffset = 0;
		int endOffset = 0;
		int minOffset = -matrix.getMaxVectorOffset(initialCell, lineType, -1);
		int maxOffset = matrix.getMaxVectorOffset(initialCell, lineType, 1);
		FigureType curFigure = FigureType.EMPTY;
		
		clearState();
		
		curCell.setXY(initialCell);
		
		while (startOffset > minOffset
				&& (curFigure == FigureType.EMPTY || curFigure == lineFigure)) {
			--startOffset;
			curCell.add(lineVector, -1);
			curFigure = matrix.get(curCell);
		}
		
		if (curFigure != FigureType.EMPTY && curFigure != lineFigure) {
			++startOffset;
		}
		
		curFigure = FigureType.EMPTY;
		// Set curCell to end of the line
		if (lineLength > 1) {
			curCell.setXY(initialCell);
			curCell.add(lineVector, lineLength - 1);
			maxOffset = maxOffset - lineLength + 1;
		}
		else {
			curCell.setXY(initialCell);
		}
		
		while (endOffset < maxOffset
				&& (curFigure == FigureType.EMPTY || curFigure == lineFigure)) {
			
			++endOffset;
			curCell.add(lineVector, 1);
			curFigure = matrix.get(curCell);
		}
		
		if (curFigure != FigureType.EMPTY && curFigure != lineFigure) {
			--endOffset;
		}
		
		extentFromStart = -startOffset;
		extentFromEnd = endOffset;
		maxPossibleLen = extentFromStart + lineLength + extentFromEnd;
	}
	
	private int extentFromStart = 0;
	private int extentFromEnd = 0;
	private int maxPossibleLen = 0;
	private PointInt curCell = new PointInt(-1, -1);
}
