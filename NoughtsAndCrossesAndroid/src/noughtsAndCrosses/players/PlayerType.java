package noughtsAndCrosses.players;

public enum PlayerType {
	LOCAL_HUMAN
	,AI1
	,AI2
	,AI2_BUGGED
	,AI3
}
