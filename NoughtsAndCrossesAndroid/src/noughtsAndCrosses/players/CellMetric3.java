package noughtsAndCrosses.players;

import java.util.*;

public class CellMetric3 implements Comparable<CellMetric3> {
	// Line must be extendible to 5 cells!
	public void addLine(LineMetric3 lineMetric) {
		if (lineMetric.getOurCellsCount() >= 2) {
			lines.add(lineMetric);
			isSorted = false;
		}
	}
	
	public void clear() {
		lines.clear();
		isSorted = true;
	}
	
	public int linesCount() {
		return lines.size();
	}
	
	public LineMetric3 getLine(int index) {
		return lines.get(index);
	}
	
	public int compareTo(CellMetric3 metric) {
		int result = 0;
		int linesNumber1, linesNumber2, minLinesNumber;
		int lineIndex = 0;
		LineMetric3 lineMetric1, lineMetric2;
		
		if (metric == null) {
			throw new NullPointerException("Cannot compare CellMetric3 object with null");
		}
		
		sort();
		metric.sort();
		
		linesNumber1 = lines.size();
		linesNumber2 = metric.lines.size();
		minLinesNumber = Math.min(linesNumber1, linesNumber2);
		
		while (lineIndex < minLinesNumber && result == 0) {
			lineMetric1 = lines.get(lineIndex);
			lineMetric2 = metric.lines.get(lineIndex);
			
			result = lineMetric1.compareTo(lineMetric2);
			++lineIndex;
		}
		
		if (result == 0) {
			if (linesNumber1 > linesNumber2) {
				result = 1;
			}
			else if (linesNumber1 < linesNumber2) {
				result = -1;
			}
		}
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		
		if (obj == null || obj.getClass() != getClass()) {
			
		}
		else {
			result = (compareTo((CellMetric3)obj) == 0);
		}
		
		return result;
	}
	
	public void sort() {
		if (!isSorted) {
			isSorted = true;
			Collections.sort(lines, linesComparator);
		}
	}
	
	private class LinesDescOrder implements Comparator<LineMetric3> {
		public int compare(LineMetric3 obj1, LineMetric3 obj2) {
			int result;
			
			// Descending order
			if (obj1 == null) {
				result = 1;
			}
			else if (obj2 == null) {
				result = -1;
			}
			else {
				result = -obj1.compareTo(obj2);
			}
			
			return result;
		}
	}
	
	//In this context line means 5 sequential cells including this cell.
	//They can be directed 1 of 4 possible ways
	private ArrayList<LineMetric3> lines = new ArrayList<LineMetric3>(4);
	private boolean isSorted = true; // Empty array is always sorted :-)
	private LinesDescOrder linesComparator = new LinesDescOrder();
}
