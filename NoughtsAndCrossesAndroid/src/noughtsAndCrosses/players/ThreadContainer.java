package noughtsAndCrosses.players;

public class ThreadContainer implements Interruptable {
	
	public ThreadContainer(Thread thread) {
		this.thread = thread;
	}
	
	public boolean isInterrupted() {
		return thread.isInterrupted();
	}
	
	private Thread thread;
}
