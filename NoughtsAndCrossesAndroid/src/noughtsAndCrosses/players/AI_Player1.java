package noughtsAndCrosses.players;

import noughtsAndCrosses.gameModel.*;
import noughtsAndCrosses.math.*;


public class AI_Player1 implements Player {
	
	public void initialize(MatrixReader<FigureType> matrix, FigureType playerFigures, Interruptable threadContainer) {
		
		this.matrix = matrix;
		this.playerFigures = playerFigures;
		this.threadContainer = threadContainer;
		
		rowsCount = matrix.rowsCount();
		columnsCount = matrix.columnsCount();
		
		analyser = new BasicGameAnalyzer(matrix);
		tools = new AI_Tools(matrix, threadContainer);
	}
	
	private void analyseCell(TurnAnalysisData analysisData, PointInt curCell) {
		CellsLine curLine = new CellsLine();
		LineInMatrix lineInMatrix = new LineInMatrix();
		int curMaxLen = analysisData.getMaxLineLength();
		int lineLen, linePosLen;
		
		for (CellsLineType lineType : allLineTypes) {
			analyser.findLine(lineType, curCell, playerFigures, curLine);
			lineLen = curLine.getLength();
			
			lineInMatrix.setUpState(matrix, curLine);
			linePosLen = lineInMatrix.getMaxPossibleLen(); 
			
			if (lineLen > curMaxLen) {
				// Else find any empty cell
				if (linePosLen >= GameModel.WIN_LINE_LENGTH) {
					curMaxLen = lineLen;
					analysisData.setMaxLineLength(lineLen);
					analysisData.setMaxPossibleLineLength(linePosLen);
				}
				
				analysisData.setOurCell(curCell);
			}
		}
	}
	
	public PointInt makeTurn(PointInt opponentTurn, int turnNumber) {
		FigureType cellFigure;
		PointInt ourTurn;
		PointInt curCell = new PointInt(0, 0);
		TurnAnalysisData analysisData = new TurnAnalysisData();
		int cellX, cellY;
		
		switch (turnNumber) {
		case 1:
			ourTurn = tools.getFirstTurn();
			analysisData.setOurCell(ourTurn);
			break;
		case 2:
			ourTurn = tools.getSecondTurn();
			if (ourTurn != null) {
				analysisData.setOurCell(ourTurn);
			}
			break;
		default:
			cellY = 0;
			
			while (cellY < rowsCount && !threadContainer.isInterrupted()) {
				curCell.setY(cellY);
				cellX = 0;
				
				while (cellX < columnsCount && !threadContainer.isInterrupted()) {
					curCell.setX(cellX);
					
					cellFigure = matrix.get(cellX, cellY);
					
					if (cellFigure == FigureType.EMPTY) {
						analyseCell(analysisData, curCell);
					}
					++cellX;
				}
				++cellY;
			}
			break;
		}
		
		return analysisData.getOurCell();
	}
	
	
	private FigureType playerFigures;
	private MatrixReader<FigureType> matrix;
	private int columnsCount;
	private int rowsCount;
	private Interruptable threadContainer;
	private BasicGameAnalyzer analyser;
	private AI_Tools tools;
	private CellsLineType[] allLineTypes = CellsLineType.values();
}
