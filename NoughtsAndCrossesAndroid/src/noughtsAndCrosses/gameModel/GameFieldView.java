package noughtsAndCrosses.gameModel;

import noughtsAndCrosses.math.*;

public interface GameFieldView {
	void updated();
	void updated(PointInt cell, FigureType newValue);
	void won();
}
