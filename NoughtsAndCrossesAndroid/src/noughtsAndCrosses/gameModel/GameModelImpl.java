package noughtsAndCrosses.gameModel;

import java.util.*;

import noughtsAndCrosses.math.*;

public class GameModelImpl implements GameModel {
	public GameModelImpl(int rows, int columns) {
		matrix = new Matrix<FigureType> (rows, columns, FigureType.EMPTY);
		matrixReader = new MatrixReaderImpl<FigureType>(matrix);
		
		views = new ArrayList<GameFieldView>(1);
		
		analiser = new BasicGameAnalyzer(matrixReader);
		analiser.searchLines();
		
		if (analiser.won()) {
			notifyViewsWon();
		}
	}
	
	// Clear game state, but don't run new game
	// Do not clear views, just update them.
	public void clear() {
		int colsCount = matrix.columnsCount();
		int rowsCount = matrix.rowsCount();
		
		// Clear matrix
		for (int cellY = 0; cellY < rowsCount; ++cellY) {
			for (int cellX = 0; cellX < colsCount; ++cellX) {
				matrix.set(cellX, cellY, FigureType.EMPTY);
			}
		}
		
		analiser.clear();
		
		turnNumber = 1;
		currentTurn = FigureType.CROSS;
		
		notifyViewsFieldUpdated();
	}
	
	public MatrixReader<FigureType> getMatrixReader() {
		return matrixReader;
	}
	
	private void set(PointInt cell, FigureType value)
			throws ArrayIndexOutOfBoundsException {
		
		matrix.set(cell, value);
		notifyViewsCellUpdated(cell, value);
	}
	
	private void turnToNextPlayer() {
		if (currentTurn == FigureType.CROSS) {
			currentTurn = FigureType.NOUGHT;
		}
		else {
			currentTurn = FigureType.CROSS;
		}
		
		++turnNumber;
	}
	
	public FigureType getCurrentTurn() {
		return currentTurn;
	}
	public int getTurnNumber() {
		return turnNumber;
	}
	public CellsLine getWinningLine() {
		return analiser.getWinningLine();
	}
	public boolean won() {
		return analiser.won();
	}
	public void registerPlayerTurn(PointInt cell) {
		FigureType previousValue = matrix.get(cell);
		
		if (previousValue == FigureType.EMPTY && !analiser.won()) {
			set(cell, currentTurn);
			
			//analiser.searchLines();
			analiser.analizePlayerMove(cell, currentTurn);
			
			if (analiser.won()) {
				notifyViewsWon();
			}
			else {
				turnToNextPlayer();
			}
		}
		/*
		else {
			throw new Exception("Incorrect player move");
		}*/
	}
	public void addView(GameFieldView aView) {
		views.add(aView);
		aView.updated();
	}
	public void removeView(GameFieldView aView) {
		views.remove(aView);
	}
	
	private void notifyViewsFieldUpdated() {
		for (GameFieldView aView : views) {
			aView.updated();
		}
	}
	
	private void notifyViewsCellUpdated(PointInt cell, FigureType newValue) {
		for (GameFieldView aView : views) {
			aView.updated(cell, newValue);
		}
	}
	
	private void notifyViewsWon() {
		for (GameFieldView aView : views) {
			aView.won();
		}
	}
	
	private Matrix<FigureType> matrix;
	private MatrixReaderImpl<FigureType> matrixReader;
	private FigureType currentTurn = FigureType.CROSS;
	private ArrayList<GameFieldView> views;
	private BasicGameAnalyzer analiser;
	private int turnNumber = 1;
}
