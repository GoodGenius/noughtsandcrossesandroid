﻿package noughtsAndCrosses.gameModel;

public enum FigureType {
	EMPTY
	,CROSS
	,NOUGHT
}
