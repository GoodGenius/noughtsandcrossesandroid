package noughtsAndCrosses.gameModel;

import noughtsAndCrosses.math.*;

public interface GameModel {
	void addView(GameFieldView aView);
	void removeView(GameFieldView aView);
	
	MatrixReader<FigureType> getMatrixReader();
	
	FigureType getCurrentTurn();
	int getTurnNumber();
	public void registerPlayerTurn(PointInt cell);
	
	boolean won();
	CellsLine getWinningLine();
	void clear();
	
	final int WIN_LINE_LENGTH = 5;
}
