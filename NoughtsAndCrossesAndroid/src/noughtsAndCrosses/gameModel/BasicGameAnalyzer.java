package noughtsAndCrosses.gameModel;

import noughtsAndCrosses.math.*;


public class BasicGameAnalyzer {
	public BasicGameAnalyzer(MatrixReader<FigureType> matrix) {
		this.matrix = matrix;
	}
	
	public void clear() {
		winningLine = null;
	}
	
	// cellFigure can be not equal to matrix.get(cellX, cellY) for hypothetical analysis
	public void findLine(CellsLineType lineType, PointInt initialCell, FigureType cellFigure,
			CellsLine curLineFound) {
		
		FigureType curFigure = cellFigure;
		PointInt curCell = new PointInt(initialCell); 
		VectorInt lineVector = CellsLine.getLineVector(lineType);
		
		int startOffset = 0;
		int endOffset = 0;
		int minOffset = -matrix.getMaxVectorOffset(initialCell, lineType, -1);
		int maxOffset = matrix.getMaxVectorOffset(initialCell, lineType, 1);
		int lineLength;
		
		while (startOffset > minOffset && curFigure == cellFigure) {
			--startOffset;
			curCell.add(lineVector, -1);
			curFigure = matrix.get(curCell);
		}
		
		if (curFigure != cellFigure) {
			++startOffset;
		} 
		
		curFigure = cellFigure;
		curCell.setXY(initialCell);
		
		while (endOffset < maxOffset && curFigure == cellFigure) {
			++endOffset;
			curCell.add(lineVector, 1);
			curFigure = matrix.get(curCell);
		}
		
		if (curFigure != cellFigure) {
			--endOffset;
		}
		
		lineLength = endOffset - startOffset + 1;
		
		curCell.setXY(initialCell);
		curCell.add(lineVector, startOffset);
		
		// curCell shouldn't be changed, after it has been passed to setUpLine(...)!!!
		curLineFound.setUpLine(lineType, cellFigure, curCell, lineLength);
	}
	
	private void processLineFound(CellsLine line) {
		if (line != null && line.getLength() >= GameModel.WIN_LINE_LENGTH
				&& winningLine == null) {
			
			winningLine = new CellsLine();
			winningLine.setUpLine(line);
		}
	}
	
	public void searchLines() {
		int rowsCount = matrix.rowsCount();
		int columnsCount = matrix.columnsCount();
		FigureType cellFigure;
		CellsLine curLineFound = new CellsLine();
		PointInt curCell = new PointInt(0, 0);
		
		for (int cellY = 0; cellY < rowsCount; ++cellY) {
			curCell.setY(cellY);
			
			for (int cellX = 0; cellX < columnsCount; ++cellX) {
				curCell.setX(cellX);
				
				cellFigure = matrix.get(cellX, cellY);
				
				if (cellFigure != FigureType.EMPTY) {
					for (CellsLineType lineType : allLineTypes) {
						findLine(lineType, curCell, cellFigure, curLineFound);
						processLineFound(curLineFound);
					}
				}
			}
		}
	}
	
	public void analizePlayerMove(PointInt cell, FigureType currentPlayer) {
		CellsLine curLineFound = new CellsLine();
		
		for (CellsLineType lineType : allLineTypes) {
			findLine(lineType, cell, currentPlayer, curLineFound);
			processLineFound(curLineFound);
		}
	}
	
	public CellsLine getWinningLine() {
		return winningLine;
	}
	
	public boolean won() {
		return (winningLine != null);
	}
	
	private MatrixReader<FigureType> matrix;
	private CellsLineType[] allLineTypes = CellsLineType.values();
	private CellsLine winningLine = null;
}
