package noughtsAndCrossesAndroid.androidViewModel;

import android.util.FloatMath;

public class GridModel {
	private int matrixRowsCount;
	private int matrixColumnsCount;
	private int marginLeft;
	private int marginTop;
	
	public GridModel(int matrixRowsCnt, int matrixColsCnt, int startX, int startY) {
		matrixRowsCount = matrixRowsCnt;
		matrixColumnsCount = matrixColsCnt;
		
		this.marginLeft = startX;
		this.marginTop = startY;
	}
	
	public int getMarginLeft() {
		return marginLeft;
	}

	public int getMarginTop() {
		return marginTop;
	}
	
	public void setStartPoint(int x, int y) {
		marginLeft = x;
		marginTop = y;
	}
	
	public int getRowsCount() {
		return matrixRowsCount;
	}
	
	public int getColumnsCount() {
		return matrixColumnsCount;
	}
	
	public void setDimensions(int rowCount, int columnCount) {
		matrixRowsCount = rowCount;
		matrixColumnsCount = columnCount;
	}
	
	public int getLinePosX(int lineNumber, int paneWidth) {
		int posX = Math.round((float)(paneWidth - 1) * (float)lineNumber / (float)matrixColumnsCount);
		
		posX += marginLeft;
		
		return posX;
	}
	
	public int getLinePosY(int lineNumber, int paneHeight) {
		int posY = Math.round((float)(paneHeight - 1) * (float)lineNumber / (float)matrixRowsCount);
		
		posY += marginTop;
		
		return posY;
	}
	
	public int getCellPosX(int pointX, int paneWidth) {
		float correctedX = (float)(pointX - marginLeft) / (float)(paneWidth - 1);
		int cellPosX = (int)Math.round(FloatMath.floor((float)matrixColumnsCount * correctedX));
		
		return cellPosX;
	}
	
	public int getCellPosY(int pointY, int paneHeight) {
		float correctedY = (float)(pointY - marginTop) / (float)(paneHeight - 1);
		int cellPosY = (int)Math.round(FloatMath.floor((float)matrixRowsCount * correctedY));
		
		return cellPosY;
	}
}
