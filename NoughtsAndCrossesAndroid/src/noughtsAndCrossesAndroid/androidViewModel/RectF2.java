package noughtsAndCrossesAndroid.androidViewModel;

import android.graphics.RectF;

public class RectF2 extends RectF {
	public RectF2() {
		super();
	}
	
	public RectF2(RectF rect) {
		super(rect);
	}
	
	public void decrease(int decreasePoints) {
		decreaseX(decreasePoints);
		decreaseY(decreasePoints);
	}
	
	public void decreaseX(int decreasePoints) {
		float widthF = width();
		int decrease2 = 2*decreasePoints;
		
		if (widthF > decrease2) {
			left += decreasePoints;
			right -= decreasePoints;
		}
		else {
			left += (float)roundDivide(widthF, 2.0F);
			right = left;
		}
	}
	
	public void decreaseY(int decreasePoints) {
		float heightF = height();
		int decrease2 = 2*decreasePoints;
		
		if (heightF > decrease2) {
			top += decreasePoints;
			bottom -= decreasePoints;
		}
		else {
			top += roundDivide(heightF, 2.0F);
			bottom = top;
		}
	}
	
	private int roundDivide(float divisible, float divisor) {
		return Math.round(divisible/divisor);
	}
}
