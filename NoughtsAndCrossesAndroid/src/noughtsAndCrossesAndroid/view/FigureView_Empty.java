package noughtsAndCrossesAndroid.view;

import android.graphics.*;
import noughtsAndCrossesAndroid.androidViewModel.RectF2;


class FigureView_Empty implements FigureInCell {
	// Uses already set color of g2 
	public void paintFigureWithColor(Canvas canvas, RectF2 cellRect, Paint paint) {
		canvas.drawRect(cellRect, paint);
	}
	
	public void paintFigure(Canvas canvas, RectF2 cellRect, Paint paint, boolean victory) {
		int savedColor = paint.getColor();
		
		paint.setColor(BACKGROUND_COLOR);
		paintFigureWithColor(canvas, cellRect, paint);
		
		paint.setColor(savedColor);
	}
	
	protected static final int BACKGROUND_COLOR = Color.BLACK;
}
