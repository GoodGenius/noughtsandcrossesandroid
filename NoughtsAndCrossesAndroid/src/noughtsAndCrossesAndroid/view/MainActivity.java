package noughtsAndCrossesAndroid.view;

import android.app.Activity;
import android.content.res.Configuration;
import android.view.*;
import android.os.Bundle;
import java.util.*;

import noughtsAndCrosses.gameModel.*;
import noughtsAndCrosses.players.PlayerType;
import noughtsAndCrossesAndroid.gameControllerMVC.GameController;

public class MainActivity extends Activity {
    private GameFieldViewImpl gameView = null;
    private GameController gameController = null;
    
    private int newGameMenuItemId = 0;
	private HashMap<FigureType, HashMap<PlayerType, MenuItem>> playerMenus;
	private HashMap<FigureType, PlayerType> playersForNewGame = new HashMap<FigureType, PlayerType>(2, 1F);
	
	//private static final int DEFAULT_ROWS = 11;
	//private static final int DEFAULT_COLUMNS = 9;
	private static final int PLAYER_MENUITEMS_COUNT = 5;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        
		playerMenus = new HashMap<FigureType, HashMap<PlayerType, MenuItem>>(2);
		
		HashMap<PlayerType, MenuItem> crosses = new HashMap<PlayerType, MenuItem>(PLAYER_MENUITEMS_COUNT);
		HashMap<PlayerType, MenuItem> noughts = new HashMap<PlayerType, MenuItem>(PLAYER_MENUITEMS_COUNT);
		
		playerMenus.put(FigureType.CROSS, crosses);
		playerMenus.put(FigureType.NOUGHT, noughts);
		
        gameView = new GameFieldViewImpl(this); // Activity is descendant of Context
        setContentView(gameView);
        //setContentView(R.layout.main);
        
		gameController = new GameController(this, gameView);
		
		gameView.post(new Runnable() {
			public void run() {
				//gameController.makeIcons();
				gameController.initialize();
		        gameController.newGame();
			}
		});
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
    	super.onConfigurationChanged(newConfig);
    	
    	updateDisplayRotation();
    }
    
    public void updateDisplayRotation() {
        Display display = getWindowManager().getDefaultDisplay();
        int rotation = display.getRotation();
        gameView.setScreenRotation(rotation);
    }
    
    private int fillPlayerMenu(SubMenu playerMenu, FigureType figures, int itemId, int groupId) {
    	HashMap<PlayerType, MenuItem> playerAlternatives = playerMenus.get(figures);
    			
		MenuItem localHumanItem	= playerMenu.add(groupId, ++itemId, itemId, "Human");
		MenuItem AI1_Item		= playerMenu.add(groupId, ++itemId, itemId, "AI Very Easy");
		MenuItem AI2_Bugged_Item= playerMenu.add(groupId, ++itemId, itemId, "AI Easy");
		MenuItem AI2_Item		= playerMenu.add(groupId, ++itemId, itemId, "AI Normal");
		MenuItem AI3_Item		= playerMenu.add(groupId, ++itemId, itemId, "AI Hard");
		
		playerMenu.setGroupCheckable(groupId, true, true);
		
		playerAlternatives.put(PlayerType.LOCAL_HUMAN, localHumanItem);
		playerAlternatives.put(PlayerType.AI1, AI1_Item);
		playerAlternatives.put(PlayerType.AI2_BUGGED, AI2_Bugged_Item);
		playerAlternatives.put(PlayerType.AI2, AI2_Item);
		playerAlternatives.put(PlayerType.AI3, AI3_Item);
		
		return itemId;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	super.onCreateOptionsMenu(menu);
    	
    	int itemId = 0;
    	MenuItem newGameItem = menu.add(Menu.NONE, ++itemId, itemId, "New Game");
    	newGameMenuItemId = newGameItem.getItemId();
    	
    	SubMenu crossesMenu = menu.addSubMenu(Menu.NONE, ++itemId, itemId, "Crosses");
    	SubMenu noughtsMenu = menu.addSubMenu(Menu.NONE, ++itemId, itemId, "Noughts");
    	
    	itemId = fillPlayerMenu(crossesMenu, FigureType.CROSS, itemId, 1);
    	itemId = fillPlayerMenu(noughtsMenu, FigureType.NOUGHT, itemId, 2);
		
    	setPlayerType(FigureType.CROSS, playersForNewGame.get(FigureType.CROSS));
    	setPlayerType(FigureType.NOUGHT, playersForNewGame.get(FigureType.NOUGHT));
    	
    	return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	int selectedMenuItemId = item.getItemId();
    	FigureType figures = null;
    	PlayerType player = null;
    	boolean itemHandled = false;
    	
    	if (selectedMenuItemId == newGameMenuItemId) {
    		gameController.newGame();
    		itemHandled = true;
    	}
    	else {
    		figures = FigureType.CROSS;
    		player = getPlayerType(figures, selectedMenuItemId);
    		
    		if (player == null) {
        		figures = FigureType.NOUGHT;
        		player = getPlayerType(figures, selectedMenuItemId);
    		}
    		
    		if (player != null) {
    			setPlayerType(figures, player);
    			itemHandled = true;
    		}
    	}
    	
    	return itemHandled;
    }

    private PlayerType getPlayerType(FigureType figures, int itemId) {
    	HashMap<PlayerType, MenuItem> playerAlternatives = playerMenus.get(figures);
		Set<PlayerType> playerKeys;
		Iterator<PlayerType> alternativesIterator;
		PlayerType curPlayerType;
		MenuItem curRadioButton;
		PlayerType result = null;
	
		if (playerAlternatives != null) {
			playerKeys = playerAlternatives.keySet();
			alternativesIterator = playerKeys.iterator();
			
			while (result == null && alternativesIterator.hasNext()) {
				curPlayerType = alternativesIterator.next();
				curRadioButton = playerAlternatives.get(curPlayerType);
				
				if (curRadioButton.getItemId() == itemId) {
					result = curPlayerType;
				}
			}
		}
		
		return result;
    }
    
    /*
	public PlayerType getPlayerType(FigureType figures) {
		HashMap<PlayerType, MenuItem> playerAlternatives = playerMenus.get(figures);
		Set<PlayerType> playerKeys;
		Iterator<PlayerType> alternativesIterator;
		PlayerType curPlayerType;
		MenuItem curRadioButton;
		PlayerType result = null;
		
		if (playerAlternatives != null) {
			playerKeys = playerAlternatives.keySet();
			alternativesIterator = playerKeys.iterator();
			
			while (result == null && alternativesIterator.hasNext()) {
				curPlayerType = alternativesIterator.next();
				curRadioButton = playerAlternatives.get(curPlayerType);
				
				if (curRadioButton.isChecked()) {
					result = curPlayerType;
				}
			}
		}
		
		return result;
	}
    */
    
	private void setPlayerType(FigureType figures, PlayerType player) {
		HashMap<PlayerType, MenuItem> playerAlternatives = playerMenus.get(figures);
		MenuItem curRadioButton;
		
		if (playerAlternatives != null) {
			curRadioButton = playerAlternatives.get(player);
			
			if (curRadioButton != null) {
				curRadioButton.setChecked(true);
				playersForNewGame.put(figures, player);
			}
		}
	}
	
	public PlayerType getPlayerTypeForNewGame(FigureType figures) {
		return playersForNewGame.get(figures);
	}

	public void setPlayerTypeForNewGame(FigureType figures, PlayerType playerType) {
		playersForNewGame.put(figures, playerType);
	}
}
