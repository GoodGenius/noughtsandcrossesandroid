package noughtsAndCrossesAndroid.view;

import android.graphics.*;
import android.util.FloatMath;

import noughtsAndCrossesAndroid.androidMath.LineEquationF;
import noughtsAndCrossesAndroid.androidViewModel.RectF2;


class FigureView_Cross extends FigureView_Empty implements FigureInCell {
	public FigureView_Cross() {
		points = new PointF[8];
		
		for (int i = 0; i < points.length; ++i) {
			points[i] = new PointF();
		}
		intersection = new PointF();
		
		diagonals = new LineEquationF[2][2];
		diagonals[0][0] = new LineEquationF(); 
		diagonals[0][1] = new LineEquationF(); 
		diagonals[1][0] = new LineEquationF(); 
		diagonals[1][1] = new LineEquationF();
		
		path = new Path();
		path.setFillType(Path.FillType.WINDING);
		path.incReserve(12); // Twelve points
	}
	
	private void calculatePoints(RectF cellRect, float lineWidthX, float lineWidthY) {
		// Top left, shifted to right
		points[0].x = cellRect.left + lineWidthX;
		points[0].y = cellRect.top;
		
		// Top right, shifted to left
		points[1].x = cellRect.right - lineWidthX;
		points[1].y = cellRect.top;
		
		// Top right, lowered down
		points[2].x = cellRect.right;
		points[2].y = cellRect.top + lineWidthY;
		
		// Bottom right, lifted up
		points[3].x = cellRect.right;
		points[3].y = cellRect.bottom - lineWidthY;
		
		// Bottom right, shifted to left
		points[4].x = cellRect.right - lineWidthX;
		points[4].y = cellRect.bottom;
		
		// Bottom left, shifted to right
		points[5].x = cellRect.left + lineWidthX;
		points[5].y = cellRect.bottom;
		
		// Bottom left, lifted up
		points[6].x = cellRect.left;
		points[6].y = cellRect.bottom - lineWidthY;
		
		// Top left, lowered down
		points[7].x = cellRect.left;
		points[7].y = cellRect.top + lineWidthY;
	}
	
	private void calculateDiagonals() {
		// Top left, shifted to right
		// Bottom right, lifted up
		diagonals[0][0].setLine(points[0], points[3]);
		
		// Top left, lowered down
		// Bottom right, shifted to left
		diagonals[0][1].setLine(points[4], points[7]);
		
		// Top right, shifted to left
		// Bottom left, lifted up
		diagonals[1][0].setLine(points[1], points[6]);
		
		// Top right, lowered down
		// Bottom left, shifted to right
		diagonals[1][1].setLine(points[2], points[5]);
	}
	
	private void calculatePath(RectF2 cellRect) {
		float width = cellRect.width();
		float height = cellRect.height();
		float lineWidth = FloatMath.ceil(Math.max(width, height)/6.0F);
		float lineWidthX = FloatMath.floor(width/3.0F);
		float lineWidthY = FloatMath.floor(height/3.0F);
		
		lineWidthX = Math.min(lineWidth, lineWidthX);
		lineWidthY = Math.min(lineWidth, lineWidthY);
		
		calculatePoints(cellRect, lineWidthX, lineWidthY);
		calculateDiagonals();
		
		path.rewind(); // Clear path, but preserve space
		path.moveTo(points[0].x, points[0].y);
		
		diagonals[0][0].findIntersection(diagonals[1][0], intersection);
		path.lineTo(intersection.x, intersection.y);
		
		path.lineTo(points[1].x, points[1].y);
		path.lineTo(points[2].x, points[2].y);
		
		diagonals[0][0].findIntersection(diagonals[1][1], intersection);
		path.lineTo(intersection.x, intersection.y);
		
		path.lineTo(points[3].x, points[3].y);
		path.lineTo(points[4].x, points[4].y);
		
		diagonals[0][1].findIntersection(diagonals[1][1], intersection);
		path.lineTo(intersection.x, intersection.y);
		
		path.lineTo(points[5].x, points[5].y);
		path.lineTo(points[6].x, points[6].y);
		
		diagonals[0][1].findIntersection(diagonals[1][0], intersection);
		path.lineTo(intersection.x, intersection.y);
		
		path.lineTo(points[7].x, points[7].y);
		path.close(); // path.lineTo(points[0].x, points[0].y);
	}
	
	@Override
	public void paintFigure(Canvas canvas, RectF2 cellRect, Paint paint, boolean victory) {
		int savedColor = paint.getColor();
		int backColor = BACKGROUND_COLOR;
		
		if (victory) {
			// Color.RED = (255, 0, 0)
			//backColor = new Color(255, 200, 200);
			// PINK = (255, 175, 175)
			//backColor = Color.MAGENTA;
			//backColor = (255, 200, 200);
			//backColor = 0xFFFFC8C8;
			backColor = 0xFFFFAFAF; // PINK
		}
		
		paint.setColor(backColor);
		super.paintFigureWithColor(canvas, cellRect, paint);
		
		calculatePath(cellRect);
		
		paint.setColor(Color.RED);
		canvas.drawPath(path, paint);
		
		paint.setColor(savedColor);
	}
	
	private PointF points[] = null;
	private PointF intersection = null;
	private LineEquationF diagonals[][] = null;
	private Path path = null;
}
