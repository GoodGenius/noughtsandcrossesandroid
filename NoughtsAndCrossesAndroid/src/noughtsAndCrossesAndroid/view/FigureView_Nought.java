package noughtsAndCrossesAndroid.view;

import android.graphics.*;
import noughtsAndCrossesAndroid.androidViewModel.RectF2;


class FigureView_Nought extends FigureView_Empty implements FigureInCell {
	@Override
	public void paintFigure(Canvas canvas, RectF2 cellRect, Paint paint, boolean victory) {
		int savedColor = paint.getColor();
		int backColor = BACKGROUND_COLOR;
		int foreColor = 0xFF0090FF;
		float width = cellRect.width();
		float height = cellRect.height();
		int lineWidth = (int)Math.ceil(Math.max(width, height)/4.0);
		int lineWidthX = (int)Math.floor(width/3.0);
		int lineWidthY = (int)Math.floor(height/3.0);
		
		lineWidthX = Math.min(lineWidth, lineWidthX);
		lineWidthY = Math.min(lineWidth, lineWidthY);
		
		if (victory) {
			// Color.CYAN = (0, 255, 255)
			//backColor = new Color(200, 255, 255);
			//backColor = Color.BLUE;
			backColor = Color.CYAN;
		}
		
		paint.setColor(backColor);
		super.paintFigureWithColor(canvas, cellRect, paint);
		
		Path path = new Path();
		path.setFillType(Path.FillType.EVEN_ODD);
		
		RectF2 innerRect = new RectF2(cellRect);
		innerRect.decreaseX(lineWidthX);
		innerRect.decreaseY(lineWidthY);
		
		path.addOval(cellRect, Path.Direction.CW);
		path.addOval(innerRect, Path.Direction.CW);
		
		//paint.setColor(Color.CYAN);
		//paint.setColor(Color.BLUE);
		paint.setColor(foreColor);
		canvas.drawPath(path, paint);
		
		paint.setColor(savedColor);
	}
	
	//private static final int NOUGHT_LINE_WIDTH = 5;
}
