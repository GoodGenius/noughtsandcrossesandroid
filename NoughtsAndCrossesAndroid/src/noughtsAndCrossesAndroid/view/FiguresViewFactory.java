package noughtsAndCrossesAndroid.view;


import java.util.*;

import noughtsAndCrosses.gameModel.*;


class FiguresViewFactory {
	public FiguresViewFactory() {
		FigureType[] allValues = FigureType.values();
		
		// We exactly know size of the map.
		cache = new HashMap<FigureType, FigureInCell>(allValues.length, 1F);
	}
	
	public FigureInCell createFigureView(FigureType figureType) {
		FigureInCell result = cache.get(figureType);
		
		if (figureType != null && result == null) {
			switch (figureType) {
			case NOUGHT:
				result = new FigureView_Nought();
				break;
			case CROSS:
				result = new FigureView_Cross();
				break;
			default:
				// Empty
				result = new FigureView_Empty();
				break;
			}
			
			cache.put(figureType, result);
		}
		
		return result; 
	}
	
	private HashMap<FigureType, FigureInCell> cache; 
}
