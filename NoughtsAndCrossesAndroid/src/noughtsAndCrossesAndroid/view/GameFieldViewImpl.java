package noughtsAndCrossesAndroid.view;

import android.view.*;
import android.content.Context;
import android.graphics.*;

import noughtsAndCrossesAndroid.gameControllerMVC.GameController;
import noughtsAndCrosses.gameModel.*;
import noughtsAndCrosses.math.*;
import noughtsAndCrossesAndroid.androidViewModel.*;

public class GameFieldViewImpl extends View implements GameFieldView {
	private int marginLeft = 1;
	private int marginRight = 0;
	private int marginTop = 1;
	private int marginBottom = 0;
	
	private GameController controller = null;
	private GameModel gameModel = null;
	private MatrixReader<FigureType> matrix = null;
	private GridModel gridModel = null;
	private Paint defaultPaint = null;
	private FiguresViewFactory figuresFactory;
	private RectF2 cellRectF = null; 
	private Rect cellRect = null;
	private int screenRotation = Surface.ROTATION_0;
	
	public GameFieldViewImpl(Context context) {
		super(context);
	}
	
	public void initialize(GameModel gameModel, GameController controller) {
		this.controller = controller;
		this.gameModel = gameModel;
		matrix = gameModel.getMatrixReader();
		gridModel = new GridModel(matrix.rowsCount(), matrix.columnsCount(), marginLeft, marginTop);
		
		defaultPaint = new Paint();
		figuresFactory = new FiguresViewFactory();
		cellRect = new Rect();
		cellRectF = new RectF2();
	}
	
	/**
	 * Sets margins to avoid green lines instead of white ones.
	 * @param portrait is current orientation Portrait or Landscape?
	 */
	public void setScreenRotation(int rotation) {
		screenRotation = rotation;
		
		switch (screenRotation) {
		case Surface.ROTATION_0:
			marginLeft = 1;
			marginRight = 0;
			marginTop = 1;
			marginBottom = 0;
			break;
		case Surface.ROTATION_90:
			marginLeft = 0;
			marginRight = 0;
			marginTop = 1;
			marginBottom = 1;
			break;
		case Surface.ROTATION_180:
			marginLeft = 0;
			marginRight = 1;
			marginTop = 0;
			marginBottom = 1;
			break;
		case Surface.ROTATION_270:
			marginLeft = 0;
			marginRight = 0;
			marginTop = 0;
			marginBottom = 0;
			break;
		}
		
		gridModel.setStartPoint(marginLeft, marginTop);
	}

	private void drawGrid(Canvas canvas,
			int colsCount, int rowsCount, int paneWidth, int paneHeight) {
		
		int pointX, pointY;
		
		//defaultPaint.setColor(Color.BLUE);
		defaultPaint.setColor(Color.WHITE);
		
		// Draw (colsCount + 1) vertical lines
		for (int cellX = 0; cellX <= colsCount; ++cellX) {
			pointX = gridModel.getLinePosX(cellX, paneWidth);
			canvas.drawLine(pointX, marginTop, pointX, paneHeight + marginTop, defaultPaint);
		}
		
		// Draw (rowsCount + 1) horizontal lines
		for (int cellY = 0; cellY <= rowsCount; ++cellY) {
			pointY = gridModel.getLinePosY(cellY, paneHeight);
			canvas.drawLine(marginLeft, pointY, paneWidth + marginLeft, pointY, defaultPaint);
		}
	}
	
	private void drawNoughtsAndCrosses(Canvas canvas,
			int colsCount, int rowsCount, int paneWidth, int paneHeight) {
		
		int x1, y1, x2, y2;
		FigureType curFigure;
		FigureInCell curView;
		
		for (int cellX = 0; cellX < colsCount; ++cellX) {
			x1 = gridModel.getLinePosX(cellX, paneWidth);
			x2 = gridModel.getLinePosX(cellX+1, paneWidth);
			
			for (int cellY = 0; cellY < rowsCount; ++cellY) {
				y1 = gridModel.getLinePosY(cellY, paneHeight);
				y2 = gridModel.getLinePosY(cellY+1, paneHeight);
				
				cellRectF.left = x1;
				cellRectF.right = x2;
				cellRectF.top = y1;
				cellRectF.bottom = y2;
				
				cellRectF.decrease(1);
				
				curFigure = matrix.get(cellX, cellY);
				curView = figuresFactory.createFigureView(curFigure);
				curView.paintFigure(canvas, cellRectF, defaultPaint, false);
			}
		}
	}
	
	private void setCellRectangle(PointInt cell) {
		int cellX = cell.getX();
		int cellY = cell.getY();
		int paneHeight = myGetHeight();
		int paneWidth = myGetWidth();
		int x1 = gridModel.getLinePosX(cellX, paneWidth);
		int x2 = gridModel.getLinePosX(cellX+1, paneWidth);
		int y1 = gridModel.getLinePosY(cellY, paneHeight);
		int y2 = gridModel.getLinePosY(cellY+1, paneHeight);
		
		cellRectF.left = x1;
		cellRectF.right = x2;
		cellRectF.top = y1;
		cellRectF.bottom = y2;
		
		cellRectF.decrease(1);
	}
	
	private void drawWinningLine(Canvas canvas, CellsLine winningLine) {
		CellsLineType lineType = winningLine.getLineType();
		FigureType figureType = winningLine.getFigureType();
		PointInt curCell = winningLine.getStartPoint();
		VectorInt lineVector = CellsLine.getLineVector(lineType);
		int lineLength = winningLine.getLength();
		
		FigureInCell curView;
		
		for (int offset = 0; offset < lineLength; ++offset) {
			setCellRectangle(curCell); // Sets cellRect
			
			curView = figuresFactory.createFigureView(figureType);
			curView.paintFigure(canvas, cellRectF, defaultPaint, true);
			
			curCell.add(lineVector, 1);
		}
	}
	
	protected void onDraw (Canvas canvas) {
		int colsCount = matrix.columnsCount();
		int rowsCount = matrix.rowsCount();
		int paneWidth = myGetWidth();
		int paneHeight = myGetHeight();
		CellsLine winningLine = gameModel.getWinningLine();
		
		drawGrid(canvas, colsCount, rowsCount, paneWidth, paneHeight);
		drawNoughtsAndCrosses(canvas, colsCount, rowsCount, paneWidth, paneHeight);
		
		if (winningLine != null) {
			drawWinningLine(canvas, winningLine);
		}
	}
	
	/*
	public void makeScreenshot(int width, int height, String fileName) {
		int colsCount = matrix.columnsCount();
		int rowsCount = matrix.rowsCount();
		CellsLine winningLine = gameModel.getWinningLine();
		
		Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		
		drawGrid(canvas, colsCount, rowsCount, width, height);
		drawNoughtsAndCrosses(canvas, colsCount, rowsCount, width, height);
		
		if (winningLine != null) {
			drawWinningLine(canvas, winningLine);
		}
		
		try {
			java.io.OutputStream out = new java.io.FileOutputStream(fileName);
			out = new java.io.BufferedOutputStream(out);
			
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
			out.close();
		}
		catch (java.io.FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (java.io.IOException e) {
			e.printStackTrace();
		}
	}
	*/
	
	public void updated() {
		invalidate(); // Invalidates whole view
	}

	public void updated(PointInt cell, FigureType newValue) {
		setCellRectangle(cell);
		
		cellRect.left = (int)cellRectF.left;
		cellRect.right = (int)cellRectF.right;
		cellRect.top = (int)cellRectF.top;
		cellRect.bottom = (int)cellRectF.bottom;
		
		invalidate(cellRect);
	}
	
	public void won() {
		invalidate();
	}
	
	
	
	public int myGetWidth() {
		return getWidth() - marginLeft - marginRight;
	}
	
	public int myGetHeight() {
		return getHeight() - marginTop - marginBottom;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		boolean eventHandled = false;
		int action = event.getAction();
		int paneHeight = myGetHeight();
		int paneWidth = myGetWidth();
		int pointX = 0;
		int pointY = 0;
		int cellX = 0;
		int cellY = 0;
		PointInt cell = null;
		
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			eventHandled = true;
			break;
		case MotionEvent.ACTION_UP:
			pointX = (int)event.getX();
			pointY = (int)event.getY();
			cellX = gridModel.getCellPosX(pointX, paneWidth);
			cellY = gridModel.getCellPosY(pointY, paneHeight);
			
			cell = new PointInt(cellX, cellY);
			
			controller.registerPlayerTurnFromView(cell);
			
			break;
		}
		
		return eventHandled;
	}
}
