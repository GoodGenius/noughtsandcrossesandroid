package noughtsAndCrossesAndroid.view;

import android.graphics.*;
import noughtsAndCrossesAndroid.androidViewModel.RectF2;

interface FigureInCell {
	void paintFigure(Canvas canvas, RectF2 cellRect, Paint paint, boolean victory);
}
