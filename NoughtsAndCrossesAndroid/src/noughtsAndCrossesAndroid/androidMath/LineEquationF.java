package noughtsAndCrossesAndroid.androidMath;

import android.graphics.PointF;

// Line y = k*x + b or x = b
public class LineEquationF {
	// Line through 2 points (x1, y1) and (x2, y2)
	public void setLine(float x1, float y1, float x2, float y2) {
		float dx = x2 - x1;
		
		if (Math.abs(dx) >= Epsilon) {
			vertical = false;
			k = (y2 - y1)/dx;
			b = (x2*y1 - x1*y2)/dx;
		}
		else {
			vertical = true;
			k = 0.0F;
			b = x1;
		}
	}
	
	public void setLine(PointF point1, PointF point2) {
		setLine(point1.x, point1.y, point2.x, point2.y);
	}
	
	public boolean findIntersection(LineEquationF line, PointF intersection) {
		float dk = line.k - k;
		boolean intersected = true;
		boolean parallel = false;
		boolean same = false;
		
		if (vertical && line.vertical) {
			if (Math.abs(b - line.b) >= Epsilon) {
				parallel = true;
			}
			else {
				same = true;
			}
		}
		else if (vertical) {
			intersection.x = b;
			intersection.y = line.k*b + line.b;
		}
		else if (line.vertical) {
			intersection.x = line.b;
			intersection.y = k*line.b + b;
		}
		else if (Math.abs(dk) >= Epsilon) {
			intersection.x = (b - line.b)/dk;
			intersection.y = (line.k*b - k*line.b)/dk;
		}
		else if (Math.abs(b - line.b) >= Epsilon) {
			parallel = true;
		}
		else {
			same = true;
		}
		
		if (parallel || same) {
			intersected = false;
			intersection.x = Float.NaN;
			intersection.y = Float.NaN;
		}
		
		return intersected;
	}
	
	public float getK() {
		return k;
	}
	public float getB() {
		return b;
	}
	public boolean isVertical() {
		return vertical;
	}
	
	// y = k*x + b
	// if vertical == true then x = b 
	private float k = 0;
	private float b = 0;
	private boolean vertical = false;
	
	private static float Epsilon = 0.001F;
}
