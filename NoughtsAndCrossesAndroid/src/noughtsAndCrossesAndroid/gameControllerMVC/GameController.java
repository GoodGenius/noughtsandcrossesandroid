package noughtsAndCrossesAndroid.gameControllerMVC;

import java.util.*;

import android.util.*;
import android.view.Display;

import noughtsAndCrosses.gameModel.*;
import noughtsAndCrosses.math.*;
import noughtsAndCrosses.players.*;
import noughtsAndCrossesAndroid.view.*;

public class GameController {
	private static final float SM_IN_INCH = 2.54F;
	private static final int MAX_ROWS = 13;
	
	private PlayersFactory playersFactory = new PlayersFactory();
	private PlayersThread playersThread = null;
	private ThreadContainer threadContainer = null;
	
	private PointInt lastTurn = new PointInt(-1, -1);
	private GameModel gameModel = null;
	private MainActivity mainActivity = null;
	private GameFieldViewImpl gameView = null;
	private HashMap<FigureType, PlayerContainer> players = new HashMap<FigureType, PlayerContainer>(2, 1F);
	
	public GameController(MainActivity activity, GameFieldViewImpl vGF) {
		mainActivity = activity;
		gameView = vGF;
		
		mainActivity.setPlayerTypeForNewGame(FigureType.CROSS, PlayerType.AI3);
		mainActivity.setPlayerTypeForNewGame(FigureType.NOUGHT, PlayerType.LOCAL_HUMAN);
	}
	
	// Called after view dimensions settled.
	public void initialize() {
		DisplayMetrics metrics = new DisplayMetrics();
		mainActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		
		int viewWidth = gameView.getWidth();
		int viewHeight = gameView.getHeight() - 1;
		float viewWidth_Sm = SM_IN_INCH * viewWidth/metrics.xdpi; // inches to centimeters
		float viewHeight_Sm = SM_IN_INCH * viewHeight/metrics.ydpi;
		float minCellWidthSm = 0.7F;
		float minCellHeightSm = 0.5F;
		boolean doSwap = false;
		
		// Same aspect ratio as physical screen, it looks not good...
		//minCellHeightSm = minCellWidthSm * (viewWidth_Sm / viewHeight_Sm);
		
		doSwap = (viewWidth >= viewHeight && minCellWidthSm < minCellHeightSm || viewWidth <= viewHeight && minCellWidthSm > minCellHeightSm);
		
		if (doSwap) {
			float swap = minCellWidthSm;
			
			minCellWidthSm = minCellHeightSm;
			minCellHeightSm = swap;
		}
		
		int rowsCount = (int)(viewHeight_Sm / minCellHeightSm);
		int columnsCount = (int)(viewWidth_Sm / minCellWidthSm);
		
		rowsCount = Math.min(rowsCount, MAX_ROWS);
		columnsCount = Math.min(columnsCount, MAX_ROWS);
		
		gameModel = new GameModelImpl(rowsCount, columnsCount);
		gameView.initialize(gameModel, this);
		gameModel.addView(gameView);
		
		mainActivity.updateDisplayRotation();
	}
	
	/*
	public void makeIcons() {
		int rowsCount = 2;
		int columnsCount = 2;
		java.io.File dir = android.os.Environment.getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_PICTURES);
		String path = dir.getPath() + "/";
		PointInt cell = new PointInt(0, 0);
		
		
		gameModel = new GameModelImpl(rowsCount, columnsCount);
		gameView.initialize(gameModel, this);
		gameModel.addView(gameView);
		
		android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(mainActivity);
		android.app.AlertDialog dialog = dialogBuilder.create();
		dialog.setMessage(path);
		dialog.show();
		
		gameModel.registerPlayerTurn(cell); // Cross
		cell.setX(1);
		gameModel.registerPlayerTurn(cell); // Nought
		cell.setY(1);
		gameModel.registerPlayerTurn(cell); // Cross
		cell.setX(0);
		gameModel.registerPlayerTurn(cell); // Nought
		
		//gameView.makeScreenshot(96, 96, path + "96.png");
		//gameView.makeScreenshot(72, 72, path + "72.png");
		//gameView.makeScreenshot(48, 48, path + "48.png");
		//gameView.makeScreenshot(36, 36, path + "36.png");
		gameView.makeScreenshot(512, 512, path + "512.png");
	}
	*/
	
	public void newGame() {
		Set<FigureType> figures;
		Iterator<FigureType> figuresIterator;
		FigureType curFigure;
		PlayerType curPlayerType;
		Player curPlayer = null;
		PlayerContainer curContainer;
		
		clear();
		
		playersThread = new PlayersThread(this, mainActivity);
		threadContainer = new ThreadContainer(playersThread);
		
		players.put(FigureType.CROSS, null);
		players.put(FigureType.NOUGHT, null);
		
		figures = players.keySet();
		figuresIterator = figures.iterator();
		
		while (figuresIterator.hasNext()) {
			curFigure = figuresIterator.next();
			curPlayerType = mainActivity.getPlayerTypeForNewGame(curFigure);
			curPlayer = playersFactory.createPlayer(curPlayerType);
			
			if (curPlayer != null) {
				curPlayer.initialize(gameModel.getMatrixReader(), curFigure, threadContainer);
			}
			
			curContainer = new PlayerContainer(curPlayer, curPlayerType);
			
			players.put(curFigure, curContainer);
		}
		
		gameView.updated();
		
		playersThread.start();
		
		runGame();
	}
	
	private void runGame() {
		int turnNumber = gameModel.getTurnNumber();
		FigureType currentTurn = gameModel.getCurrentTurn();
		PlayerContainer curContainer = players.get(currentTurn);
		Player currentPlayer;
		
		if (curContainer != null
			&& curContainer.getPlayerType() != PlayerType.LOCAL_HUMAN
			&& !gameModel.won()) {
			
			currentPlayer = curContainer.getPlayer();
			playersThread.makeNextTurn(currentPlayer, lastTurn, turnNumber);
		}
	}
	
	// Call this method only from main thread.
	public void registerPlayerTurnFromPlayersThread(PointInt cell) {
		FigureType currentTurn = gameModel.getCurrentTurn();
		PlayerContainer currentPlayerCont = players.get(currentTurn);
		
		if (cell != null && currentPlayerCont != null
			&& currentPlayerCont.getPlayerType() != PlayerType.LOCAL_HUMAN) {
			
			lastTurn.setXY(cell);
			gameModel.registerPlayerTurn(lastTurn);
			runGame();
		}
	}
	
	// Call this method only from main thread.
	public void registerPlayerTurnFromView(PointInt cell) {
		FigureType currentTurn = gameModel.getCurrentTurn();
		PlayerContainer currentPlayerCont = players.get(currentTurn);
		
		if (cell != null && currentPlayerCont != null
			&& currentPlayerCont.getPlayerType() == PlayerType.LOCAL_HUMAN) {
			
			lastTurn.setXY(cell);
			gameModel.registerPlayerTurn(lastTurn);
			runGame();
		}
	}
	
	private void clear() {
		players.clear();
		gameModel.clear();
		lastTurn.setX(-1);
		lastTurn.setY(-1);
		
		if (playersThread != null) {
			playersThread.interrupt();
			playersThread = null;
		}
	}
}
