package noughtsAndCrossesAndroid.gameControllerMVC;

import android.app.Activity;
import java.util.concurrent.locks.*;

import noughtsAndCrosses.math.*;
import noughtsAndCrosses.players.*;

class PlayersThread extends Thread {
	public PlayersThread(GameController controller, Activity mainActivity) {
		this.controller = controller;
		this.mainActivity = mainActivity;
		
		threadLock = new ReentrantLock();
		readyForNextTurnCondition = threadLock.newCondition(); 
	} 
	
	private void registerTurn(final PointInt playerTurn) {
		mainActivity.runOnUiThread(new Runnable() {
			public void run() {
				controller.registerPlayerTurnFromPlayersThread(playerTurn);
			}
		});
	}
	
	
	@Override
	public void run() {
		PointInt playerTurn;
		
		while (!isInterrupted() && !finishThread) {
			threadLock.lock();
			try {
				while (!isInterrupted() && !readyForNextTurn) {
					readyForNextTurnCondition.await();
				}
				readyForNextTurn = false;
				
				LockSupport.parkNanos(300000000);
				//LockSupport.parkNanos(1000000000);
				playerTurn = player.makeTurn(opponentTurn, turnNumber);
				
				if (!isInterrupted()) {
					registerTurn(playerTurn);
				}
			}
			catch (InterruptedException e) {
				finishThread = true;
			}
			finally {
				threadLock.unlock();
			}
		}
	}
	
	// Called form another thread
	public void makeNextTurn(Player player, PointInt opponentTurn, int turnNumber) {
		threadLock.lock();
		try {
			this.player = player;
			this.opponentTurn = opponentTurn;
			this.turnNumber = turnNumber;
			readyForNextTurn = true;
			
			readyForNextTurnCondition.signalAll();
		}
		finally {
			threadLock.unlock();
		}
	}
	
	
	private GameController controller = null;
	Activity mainActivity = null;
	private Player player = null;
	private PointInt opponentTurn = null;
	private int turnNumber;
	
	private ReentrantLock threadLock = null;
	private Condition readyForNextTurnCondition = null;
	private boolean readyForNextTurn = false;
	private boolean finishThread = false;
}
